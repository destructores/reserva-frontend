# -*- coding:utf-8 -*-
SALAS = [
	{
		"codigo": 1, 
		"nombre":u"Belmond Miraflores Park", 
		"descripcion":u"Belmond ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		"capacidad":50, 
		"foto-url":u"img/sala01.jpg",
		"caracteristicas ": ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		"submit": {"url":u"#"}
	},
	{
		"codigo": 2, 
		"nombre":u"Delfines Hotel & Casino", 
		"descripcion":u"Delfines Hotel & Casino ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		"capacidad":50, 
		"foto-url":u"img/sala02.jpg",
		"caracteristicas ": ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		"submit": {"url":u"#"}
	},
	{
		"codigo": 3, 
		"nombre":u"JW Marriott Hotel Lima", 
		"descripcion":u"JW Marriott Hotel Lima ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		"capacidad":50, 
		"foto-url":u"img/sala03.jpg",
		"caracteristicas ": ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		"submit": {"url":u"#"}
	},
	{
		"codigo": 4, 
		"nombre":u"Sheraton Lima Hotel & Convention Center", 
		"descripcion":u"Sheraton Lima Hotel & Convention Center ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		"capacidad":50, 
		"foto-url":u"img/sala04.jpg",
		"caracteristicas ": ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		"submit": {"url":u"#"}
	},
	{
		"codigo": 5, 
		"nombre":u"Courtyard Lima Miraflores", 
		"descripcion":u"Courtyard Lima Miraflores ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		"capacidad":60, 
		"foto-url":u"img/sala05.jpg",
		"caracteristicas ": ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		"submit": {"url":u"#"}
	},
]