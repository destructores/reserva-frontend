from time import sleep
from flask import Flask
from flask import render_template, request
from strings import SALAS
from proxy import SalaClient
from models import FormBuscar

app = Flask(__name__)

@app.route('/',methods=['GET', 'POST'])
def login():
	if request.method == 'POST':
		return render_template("components/form_salas.html")
	else:
		return render_template("components/login.html")

@app.route('/salas',methods=['GET'])
def salas():
	zona = "surco"
	return render_template("pages/salas.html",zona=zona)

@app.route('/salas/listar', methods=['POST'])
def listar_salas():
    form = FormBuscar(**dict(request.form))
    zona = request.form["zona"]
    salas = SalaClient.obtener_salas_por_zona(zona)
    return render_template("pages/salas_encontradas.html",salas=salas,zona=zona)

@app.route('/salas/reservadas',methods=["GET"])
def mis_reservas():
	return render_template("pages/salas_reservadas.html")

@app.route('/salas/<int:sala_id>',methods=["GET","POST"])
def sala(sala_id):
	if request.method == "POST":
		return "post"
	else:
		sala = SALAS[int(sala_id)-1]#{"caracteristicas":["a","b"]}
		cantidad_personas = range(20,40)
		return render_template("components/sala.html",sala=sala,cantidad_personas=cantidad_personas)

if __name__ == '__main__':
    app.run(debug=True)