class Sala(object):
	def __init__(
		self,
		Capacidad,
		Contacto,
		Correo,
		Direccion,
		Distrito,
		Id,
		Nombre,
		Referencia,
		Telefono,
		Zona
		):
		self.Capacidad = Capacidad
		self.Contacto = Contacto
		self.Correo = Correo
		self.Direccion = Direccion
		self.Distrito = Distrito
		self.Id = Id
		self.Nombre = Nombre
		self.Referencia = Referencia
		self.Telefono = Telefono
		self.Zona = Zona

	def __str__(self):
		return self.Nombre

	def __unicode__(self):
		return self.Nombre

class FormBuscar(object):
	def __init__(self,zona,fecha_inicio,fecha_fin,hora_inicio,hora_fin):
		self.zona = zona
		self.fecha_inicio = fecha_inicio
		self.fecha_fin = fecha_fin
		self.hora_inicio = hora_inicio
		self.hora_fin = hora_fin
