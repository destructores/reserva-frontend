from suds.client import Client, WebFault
from models import Sala
import os

class SalaClient(object):
	@classmethod
	def obtener_salas_por_zona(self,zona):
		#url='http://localhost:5000/static/wsdl/SalasService.WSDL'
		url='http://localhost:52670/descriptor/SalasService.WSDL'
		client = Client(url=url)
		try:
			response = client.service.ObtenerSala(zona)
			print response
			salasInstances = [response]
			salasDict = [dict(i) for i in salasInstances]
			salas = [Sala(**d) for d in salasDict]
			return salas
		except WebFault, f:
			print f
			return []
		
if __name__ == "__main__":
	print SalaClient.obtener_salas_por_zona("SURCO")