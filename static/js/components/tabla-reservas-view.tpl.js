(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['tabla-reservas-view.html'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "		<tr>\r\n			<td>"
    + alias4(((helper = (helper = helpers.codigo || (depth0 != null ? depth0.codigo : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"codigo","hash":{},"data":data}) : helper)))
    + "</td>\r\n			<td>"
    + alias4(((helper = (helper = helpers.nombre || (depth0 != null ? depth0.nombre : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"nombre","hash":{},"data":data}) : helper)))
    + "</td>\r\n			<td>"
    + alias4(((helper = (helper = helpers.capacidad || (depth0 != null ? depth0.capacidad : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"capacidad","hash":{},"data":data}) : helper)))
    + "</td>\r\n			<td>"
    + alias4(((helper = (helper = helpers.fecha || (depth0 != null ? depth0.fecha : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"fecha","hash":{},"data":data}) : helper)))
    + "</td>\r\n			<td>"
    + alias4(((helper = (helper = helpers.hora_inicio || (depth0 != null ? depth0.hora_inicio : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hora_inicio","hash":{},"data":data}) : helper)))
    + "</td>\r\n			<td>"
    + alias4(((helper = (helper = helpers.hora_fin || (depth0 != null ? depth0.hora_fin : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"hora_fin","hash":{},"data":data}) : helper)))
    + "</td>\r\n			<td>\r\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.coffee_break : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "</td>\r\n		</tr>\r\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "				<span class=\"glyphicon glyphicon-ok text-success\" aria-hidden=\"true\"></span> \r\n";
},"4":function(container,depth0,helpers,partials,data) {
    return "				<span class=\"glyphicon glyphicon-minus\" aria-hidden=\"true\"></span>	\r\n				";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<h2>MIS RESERVAS</h2>\r\n\r\n<table class=\"table table-hovered table-responsive table-bordered\">\r\n	<header>\r\n		<tr>\r\n			<th>N°</th>\r\n			<th>SALA</th>\r\n			<th>CAPACIDAD</th>\r\n			<th>FECHA</th>\r\n			<th>HORA INICIO</th>\r\n			<th>HORA FIN</th>\r\n			<th>COFFEE BREAK</th>\r\n		</tr>\r\n	</header>\r\n	<body>\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.reservas : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	</body>\r\n\r\n</table>";
},"useData":true});
})();