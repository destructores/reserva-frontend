(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['oficina-view.html'] = template({"1":function(container,depth0,helpers,partials,data) {
    return "          <li class=\"list-group-item\">"
    + container.escapeExpression(container.lambda(depth0, depth0))
    + "</li>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression, alias2=depth0 != null ? depth0 : {}, alias3=helpers.helperMissing, alias4="function";

  return "<form action=\""
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.submit : depth0)) != null ? stack1.url : stack1), depth0))
    + "\" method=\"POST\">\r\n<div class=\"panel panel-default\">\r\n  <!-- Default panel contents -->\r\n  <div class=\"panel-heading\"><strong>"
    + alias1(((helper = (helper = helpers.nombre || (depth0 != null ? depth0.nombre : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"nombre","hash":{},"data":data}) : helper)))
    + "</strong></div>\r\n  <div class=\"panel-body\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-6\">\r\n        <img src=\""
    + alias1(((helper = (helper = helpers["foto-url"] || (depth0 != null ? depth0["foto-url"] : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"foto-url","hash":{},"data":data}) : helper)))
    + "\" class=\"img img-responsive\">\r\n        <hr>\r\n        <p>\r\n          "
    + alias1(((helper = (helper = helpers.descripcion || (depth0 != null ? depth0.descripcion : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"descripcion","hash":{},"data":data}) : helper)))
    + "\r\n        </p>\r\n      </div>\r\n      <div class=\"col-md-6\">\r\n        <h5>Caracteristicas</h5>\r\n      <!-- List group -->\r\n      <ul class=\"list-group\">\r\n          <li class=\"list-group-item\">Capacidad : "
    + alias1(((helper = (helper = helpers.capacidad || (depth0 != null ? depth0.capacidad : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"capacidad","hash":{},"data":data}) : helper)))
    + "</li>\r\n"
    + ((stack1 = helpers.each.call(alias2,(depth0 != null ? depth0.caracteristicas : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "      </ul>\r\n      <hr>\r\n        <div class=\"col-md-12\">\r\n          <form action=\"\" method=\"POST\">\r\n          <input type=\"text\" hidden value=\""
    + alias1(((helper = (helper = helpers.codigo || (depth0 != null ? depth0.codigo : depth0)) != null ? helper : alias3),(typeof helper === alias4 ? helper.call(alias2,{"name":"codigo","hash":{},"data":data}) : helper)))
    + "\"/> \r\n           <div class=\"checkbox\">\r\n            <label>\r\n              <input type=\"checkbox\"> Coffee Break\r\n            </label>\r\n        </div>\r\n          <div class=\"form-group\">\r\n              <label for=\"cantidad\">Cantidad de personas</label>\r\n              <select class=\"form-control\" name=\"cantidad\" id=\"cantidad\">\r\n              <option>10</option>\r\n              <option>11</option>\r\n              <option>12</option>\r\n              <option>13</option>\r\n              <option>14</option>\r\n              <option>15</option>\r\n              <option>16</option>\r\n              <option>17</option>\r\n              <option>18</option>\r\n              <option>19</option>\r\n              <option>20</option>\r\n              <option>21</option>\r\n              <option>22</option>\r\n              <option>23</option>\r\n              <option>24</option>\r\n              <option>25</option>\r\n              <option>26</option>\r\n              <option>27</option>\r\n              <option>28</option>\r\n              <option>29</option>\r\n              <option>30</option>\r\n              <option>31</option>\r\n              <option>32</option>\r\n              <option>33</option>\r\n              <option>34</option>\r\n              <option>35</option>\r\n              <option>36</option>\r\n              <option>37</option>\r\n              <option>38</option>\r\n              <option>39</option>\r\n              <option>40</option>\r\n            </select>\r\n          </div>\r\n          </form>\r\n\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"panel-footer\">\r\n    <div class=\"row\">\r\n      <div class=\"col-md-12\">\r\n        <button class=\"btn btn-primary btn-lg pull-right\" type=\"submit\"> Reservar  </button>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n</form>";
},"useData":true});
})();