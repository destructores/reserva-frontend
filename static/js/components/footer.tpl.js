(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['footer.html'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper;

  return "<footer>\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-12\">\r\n            <p>Copyright &copy; "
    + container.escapeExpression(((helper = (helper = helpers.descripcion || (depth0 != null ? depth0.descripcion : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"descripcion","hash":{},"data":data}) : helper)))
    + "</p>\r\n        </div>\r\n    </div>\r\n</footer>";
},"useData":true});
})();