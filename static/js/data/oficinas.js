var oficinas = [
	{
		codigo: 1, 
		nombre:"Belmond Miraflores Park", 
		descripcion:"Belmond ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		capacidad:50, 
		"foto-url":"img/sala01.jpg",
		caracteristicas : ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		submit: {url:"#"}
	},
	{
		codigo: 2, 
		nombre:"Delfines Hotel & Casino", 
		descripcion:"Delfines Hotel & Casino ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		capacidad:50, 
		"foto-url":"img/sala02.jpg",
		caracteristicas : ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		submit: {url:"#"}
	},
	{
		codigo: 3, 
		nombre:"JW Marriott Hotel Lima", 
		descripcion:"JW Marriott Hotel Lima ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		capacidad:50, 
		"foto-url":"img/sala03.jpg",
		caracteristicas : ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		submit: {url:"#"}
	},
	{
		codigo: 4, 
		nombre:"Sheraton Lima Hotel & Convention Center", 
		descripcion:"Sheraton Lima Hotel & Convention Center ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		capacidad:50, 
		"foto-url":"img/sala04.jpg",
		caracteristicas : ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		submit: {url:"#"}
	},
	{
		codigo: 5, 
		nombre:"Courtyard Lima Miraflores", 
		descripcion:"Courtyard Lima Miraflores ofrece lugares destacados para cada reunión proporcionando una experiencia única. Cuenta con un equipamiento tecnológico de telepresencia CISCO.", 
		capacidad:60, 
		"foto-url":"img/sala05.jpg",
		caracteristicas : ["Cofee break","Con estacionamiento", "Seguridad 24x7"],
		submit: {url:"#"}
	},
];