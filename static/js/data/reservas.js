var reservas = [
	{
		codigo: 1, 
		nombre:"Sheraton Lima Hotel & Convention Center", 
		capacidad:40,
		fecha:"06/09/2016",
		hora_inicio:"21:00", 
		hora_fin:"23:00",
		coffee_break:false 
	},
	{
		codigo: 2, 
		nombre:"Courtyard Lima Miraflores", 
		capacidad:36,
		fecha:"07/09/2016",
		hora_inicio:"18:00", 
		hora_fin:"20:00",
		coffee_break:true 
	},
	{
		codigo: 3, 
		nombre:"Belmond Miraflores Park", 
		capacidad:30,
		fecha:"08/09/2016",
		hora_inicio:"12:00", 
		hora_fin:"18:00", 
		coffee_break:true 
	},
];